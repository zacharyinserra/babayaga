﻿Imports System.ComponentModel

Public Class viewModelucSupportFileTool

	Implements INotifyPropertyChanged

	Public Sub New()
		Try
			propDefFileLocation = My.Settings.DefFileLocation
			propServerName = My.Settings.ServerName
			propDatabaseName = My.Settings.DatabaseName
			propUseWindowsAuth = My.Settings.UseWindowsAuth

			If useWindowsAuth = False Then
				propUsername = My.Settings.WindowsUsername
				propPassword = My.Settings.WindowsPassword
			End If
		Catch : End Try
	End Sub

	Private defFileLocation As String
	Private serverName As String
	Private databaseName As String
	Private useWindowsAuth As Boolean
	Private username As String
	Private password As String
	Private prog As Integer

	Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
	'
	'
	'Properties
	'
	'
	Public Property propDefFileLocation As String
		Get
			Return defFileLocation
		End Get
		Set(value As String)
			defFileLocation = value
			NotifyPropertyChanged("propDefFileLocation")
		End Set
	End Property

	Public Property propServerName As String
		Get
			Return serverName
		End Get
		Set(value As String)
			serverName = value
			NotifyPropertyChanged("propServerName")
		End Set
	End Property

	Public Property propDatabaseName As String
		Get
			Return databaseName
		End Get
		Set(value As String)
			databaseName = value
			NotifyPropertyChanged("propDatabaseName")
		End Set
	End Property

	Public Property propUseWindowsAuth As Boolean
		Get
			Return useWindowsAuth
		End Get
		Set(value As Boolean)
			useWindowsAuth = value
			NotifyPropertyChanged("propUseWindowsAuth")
		End Set
	End Property

	Public Property propUsername As String
		Get
			Return username
		End Get
		Set(value As String)
			username = value
			NotifyPropertyChanged("propUsername")
		End Set
	End Property

	Public Property propPassword As String
		Get
			Return password
		End Get
		Set(value As String)
			password = value
			NotifyPropertyChanged("propPassword")
		End Set
	End Property

	Public Property propProgress As Integer
		Get
			Return prog
		End Get
		Set(value As Integer)
			prog = value
			NotifyPropertyChanged("propProgress")
		End Set
	End Property

	Private Sub NotifyPropertyChanged(ByVal info As String)
		RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
	End Sub

End Class
