﻿Imports System.ComponentModel

Public Class viewModelucEncryptDecrypt

	Implements INotifyPropertyChanged

	Private encryptInput As String
	Private encryptOutput As String
	Private decryptInput As String
	Private decryptOutput As String

	Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
	'
	'
	'Properties
	'
	'
	Public Property propEncryptInput As String
		Get
			Return encryptInput
		End Get
		Set
			encryptInput = Value
			NotifyPropertyChanged("propEncryptInput")
		End Set
	End Property

	Public Property propEncryptOutput As String
		Get
			Return encryptOutput
		End Get
		Set
			encryptOutput = Value
			NotifyPropertyChanged("propEncryptOutput")
		End Set
	End Property

	Public Property propDecryptInput As String
		Get
			Return decryptInput
		End Get
		Set
			decryptInput = Value
			NotifyPropertyChanged("propDecryptInput")
		End Set
	End Property

	Public Property propDecryptOutput As String
		Get
			Return decryptOutput
		End Get
		Set
			decryptOutput = Value
			NotifyPropertyChanged("propDecryptOutput")
		End Set
	End Property

	Private Sub NotifyPropertyChanged(ByVal info As String)
		RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
	End Sub
End Class
