﻿Imports System.ComponentModel

Public Class viewModelucDUGONG

	Implements INotifyPropertyChanged
	Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
	Public Sub New()
		propProduct = My.Settings.Product
		propBackupDest = My.Settings.BackupDest
	End Sub
	'
	'gridConnect
	'
	Private product As String
	Private backupDest As String
	Private prog As Integer

	Public Property propProduct As String
		Get
			Return product
		End Get
		Set
			product = Value
			NotifyPropertyChanged("propProduct")
		End Set
	End Property

	Public Property propBackupDest As String
		Get
			Return backupDest
		End Get
		Set
			backupDest = Value
			NotifyPropertyChanged("propBackupDest")
		End Set
	End Property

	Public Property propProg As Integer
		Get
			Return prog
		End Get
		Set
			prog = Value
			NotifyPropertyChanged("propProg")
		End Set
	End Property
	'
	'gridQueryBuilder
	'
	Private tables As New List(Of String)
	Private columns As New List(Of String)
	Private whereColumns As New List(Of String)
	Private whereValues As New List(Of String)

	Public Property propTables As List(Of String)
		Get
			Return tables
		End Get
		Set
			tables = Value
			NotifyPropertyChanged("propTables")
		End Set
	End Property

	'Public Property propColumns As List(Of String)
	'	Get
	'		Return columns
	'	End Get
	'	Set
	'		columns = Value
	'		NotifyPropertyChanged("propColumns")
	'	End Set
	'End Property

	'Public Property propWhereColumns As List(Of String)
	'	Get
	'		Return whereColumns
	'	End Get
	'	Set
	'		whereColumns = Value
	'		NotifyPropertyChanged("propWhereColumns")
	'	End Set
	'End Property

	'Public Property propWhereValues As List(Of String)
	'	Get
	'		Return whereValues
	'	End Get
	'	Set
	'		whereValues = Value
	'		NotifyPropertyChanged("propWhereValues")
	'	End Set
	'End Property

	'
	'gridReview
	'
	Private query As String

	Public Property propQuery As String
		Get
			Return query
		End Get
		Set
			query = Value
			NotifyPropertyChanged("propQuery")
		End Set
	End Property
	Private Sub NotifyPropertyChanged(ByVal info As String)
		RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
	End Sub

End Class
