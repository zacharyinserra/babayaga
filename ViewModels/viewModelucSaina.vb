﻿Imports System.ComponentModel

Public Class viewModelucSaina

	Implements INotifyPropertyChanged
	Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

	Private files As New List(Of String)
	'
	'
	'Properties
	'
	'
	Public Property propFiles As List(Of String)
		Get
			Return files
		End Get
		Set
			files = Value
			NotifyPropertyChanged("propFiles")
		End Set
	End Property

	Private Sub NotifyPropertyChanged(ByVal info As String)
		RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
	End Sub

End Class
