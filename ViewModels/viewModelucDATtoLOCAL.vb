﻿Imports System.ComponentModel

Public Class viewModelucDATtoLOCAL

    Implements INotifyPropertyChanged

    Private prog As Integer

    Public Property propProgress As Double
        Get
            Return prog
        End Get
        Set(value As Double)
            prog = value
            NotifyPropertyChanged("propProgress")
        End Set
    End Property

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

End Class
