﻿Imports System.ComponentModel

Public Class viewModelucExtendo

	Implements INotifyPropertyChanged

	Private serverName As String = Environment.MachineName
	Private encodedInfo As String
	Private status As String
	Private prog As Integer

	Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
	'
	'
	'Properties
	'
	'
	Public Property propServerName As String
		Get
			Return serverName
		End Get
		Set(value As String)
			serverName = value
			NotifyPropertyChanged("propServerName")
		End Set
	End Property

	Public Property propEncodedInfo As String
		Get
			Return encodedInfo
		End Get
		Set(value As String)
			encodedInfo = value
			NotifyPropertyChanged("propEncodedInfo")
		End Set
	End Property

	Public Property propStatus As String
		Get
			Return status
		End Get
		Set(value As String)
			status = value
			NotifyPropertyChanged("propStatus")
		End Set
	End Property

	Public Property propProg As Integer
		Get
			Return prog
		End Get
		Set(value As Integer)
			prog = value
			NotifyPropertyChanged("propProg")
		End Set
	End Property

	Private Sub NotifyPropertyChanged(ByVal info As String)
		RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
	End Sub

End Class
