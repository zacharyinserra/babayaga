﻿Imports System.Data
Imports babayaga.clsTrialDetails

Friend Class clsDataAccess
	Private Property propConnectionString As String
	Private Property propType As ConnectionTypeEnum

	Public Sub New(ByVal connectionString As String, ByVal type As ConnectionTypeEnum)
		propConnectionString = connectionString
		propType = type
	End Sub

	Public Function executeQuery(ByVal query As String, ByVal parameters As List(Of KeyValuePair(Of String, Object))) As Boolean
		If propType = ConnectionTypeEnum.NATIVE Then
			Return sqlExecuteQuery(query, parameters)
		ElseIf propType = ConnectionTypeEnum.ODBC Then
			Return odbcExecuteQuery(query, parameters)
		Else
			Return datExecuteQuery(query)
		End If
	End Function

	Private Function datExecuteQuery(ByVal query As String) As Boolean
		Using con As Data.OleDb.OleDbConnection = New Data.OleDb.OleDbConnection(propConnectionString)
			con.Open()

			Using com As Data.OleDb.OleDbCommand = New Data.OleDb.OleDbCommand(query, con)
				com.ExecuteNonQuery()
			End Using

			con.Close()
		End Using

		Return True
	End Function

	Private Function odbcExecuteQuery(ByVal query As String, ByVal parameters As List(Of KeyValuePair(Of String, Object))) As Boolean
		Using con As Data.Odbc.OdbcConnection = New Data.Odbc.OdbcConnection(propConnectionString)
			con.Open()

			Using com As Data.Odbc.OdbcCommand = New Data.Odbc.OdbcCommand(query, con)

				For Each kvp In parameters
					com.Parameters.AddWithValue(kvp.Key, kvp.Value)
				Next

				com.ExecuteNonQuery()
			End Using

			con.Close()
			Return True
		End Using
	End Function

	Private Function sqlExecuteQuery(ByVal query As String, ByVal parameters As List(Of KeyValuePair(Of String, Object))) As Boolean
		Using con As Data.SqlClient.SqlConnection = New Data.SqlClient.SqlConnection(propConnectionString)
			con.Open()

			Using com As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(query, con)

				If parameters IsNot Nothing Then

					For Each kvp In parameters
						com.Parameters.AddWithValue(kvp.Key, kvp.Value)
					Next
				End If

				com.ExecuteNonQuery()
			End Using

			con.Close()
			Return True
		End Using
	End Function

	Public Function getRecords(ByVal query As String) As DataTable
		If propType = ConnectionTypeEnum.ODBC Then
			Return getRecordsODBC(query)
		ElseIf propType = ConnectionTypeEnum.NATIVE Then
			Return getRecordsSQL(query)
		Else
			Return getRecordsDAT(query)
		End If
	End Function

	Private Function getRecordsDAT(ByVal query As String) As DataTable
		Using con As Data.OleDb.OleDbConnection = New Data.OleDb.OleDbConnection(propConnectionString)
			con.Open()

			Using da As Data.OleDb.OleDbDataAdapter = New Data.OleDb.OleDbDataAdapter(query, con)
				Dim dt = New DataTable
				da.Fill(dt)
				con.Close()
				Return dt
			End Using
		End Using
	End Function

	Private Function getRecordsODBC(ByVal query As String) As DataTable
		Using con As Data.Odbc.OdbcConnection = New Data.Odbc.OdbcConnection(propConnectionString)
			con.Open()

			Using da As Data.Odbc.OdbcDataAdapter = New Data.Odbc.OdbcDataAdapter(query, con)
				Dim dt = New DataTable
				da.Fill(dt)
				con.Close()
				Return dt
			End Using
		End Using
	End Function

    Private Function getRecordsSQL(ByVal query As String) As DataTable
        Using con As Data.SqlClient.SqlConnection = New Data.SqlClient.SqlConnection(propConnectionString)
            con.Open()

            Using da As Data.SqlClient.SqlDataAdapter = New Data.SqlClient.SqlDataAdapter(query, con)
                Dim dt = New DataTable
                da.Fill(dt)
                con.Close()
                Return dt
            End Using
        End Using
    End Function
End Class
