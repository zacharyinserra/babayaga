﻿Imports System.Data
Imports System.Data.Odbc
Imports System.Data.SqlClient

Public Class clsDataMigrator

	Public Sub New(Optional ByVal databaseType As dbType = 1)
		propDatabaseType = databaseType
	End Sub

	Enum dbType
		ODBC = 0
		LOCAL = 1
	End Enum

	Dim conType As dbType
	Dim tablesToInclude As DataTable = Nothing

	Public Property propDatabaseType() As dbType
		Get
			Return ConType
		End Get
		Set(ByVal value As dbType)
			ConType = value
		End Set
	End Property

	Public Property propTablesToInclude As DataTable
		Get
			Return tablesToInclude
		End Get
		Set(value As DataTable)
			tablesToInclude = value
		End Set
	End Property

	Public Function Import(ByVal destDSN As String, ByVal defFileLocation As String) As Boolean
		ucSupportFileTool.vmSuppFileTool.propProgress = 20

        Dim chk As Boolean = True

        Try
			'Build dataset from def file
			Dim ds As DataSet = New DataSet
			ds.ReadXml(defFileLocation, XmlReadMode.ReadSchema)
            chk = ImportFromDataSet(destDSN, ds)

        Catch ex As Exception
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Import Failed")
            chk = False
        End Try

		Return chk
	End Function

	Public Overloads Function Export(ByVal srcDSN As String) As DataSet
		Try
			Dim ds As DataSet = New DataSet
			Dim err As Exception

			If propDatabaseType = dbType.ODBC Then
				ExportToDataSet(srcDSN, ds, err)
			ElseIf propDatabaseType = dbType.LOCAL Then
				ExportToDataSet(ds, err)
			End If

			Return ds
		Catch ex As Exception
			Return Nothing
		End Try
	End Function

	Public Overloads Function Export(ByVal srcDSN As String, ByVal fileName As String, ByRef errInfo As Exception) As Boolean
		Dim rc As Boolean = False
		Try
			Dim ds As DataSet = New DataSet

			If propDatabaseType = dbType.ODBC Then
				ExportToDataSet(srcDSN, ds, errInfo)
			Else
				ExportToDataSet(ds, errInfo)
			End If

			'Write the dataset (including the table schema) as XML to the supplied file
			ds.WriteXml(fileName, XmlWriteMode.WriteSchema)

			rc = True
		Catch
		End Try

		Return rc
	End Function

    Private Function ImportFromDataSet(ByVal destDSN As String, ByRef ds As DataSet) As Boolean
        Dim chk As Boolean = True
        Dim sqlConn As SqlConnection = New SqlConnection(destDSN)
        Dim increment As Double = 75 / ds.Tables.Count

        Try
            sqlConn.Open()

            'Drop table and add new data in
            For Each dt As DataTable In ds.Tables
                DropTable(dt.TableName, sqlConn)

                If CreateTable(dt, sqlConn) Then
                    ImportDataToTable(dt, sqlConn)
                End If
                ucSupportFileTool.vmSuppFileTool.propProgress += increment
            Next

            sqlConn.Close()

        Catch ex As Exception
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Import from dataset failed")
            chk = False
        Finally
            sqlConn.Dispose()
        End Try
        Return chk
    End Function

    Private Sub DropTable(ByVal tableName As String, ByVal sqlConn As SqlConnection)
		Try
			Dim deleteCmd As SqlCommand = New SqlCommand("DROP TABLE [" + tableName + "]", sqlConn)
			deleteCmd.ExecuteNonQuery()
			'Ignore errors as table may not exist
		Catch : End Try
	End Sub

	Private Function CreateTable(ByVal dt As DataTable, ByVal sqlconn As SqlConnection) As Boolean
		Dim chk As Boolean = True

		'Execute SQL CREATE statement
		Try
			Dim createSQL As String = BuildCreateTableSQL(dt.TableName, dt)
			Dim createCmd As SqlCommand = New SqlCommand(createSQL, sqlconn)
			createCmd.ExecuteNonQuery()

		Catch ex As Exception
			MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Create table failed")
		End Try

		Return chk
	End Function

	Private Function ImportDataToTable(ByVal dt As DataTable, ByVal sqlconn As SqlConnection) As Boolean
		Dim returnValue As Boolean = True

		Try
			Dim bulkCopy As SqlBulkCopy = New SqlBulkCopy(sqlconn)
			bulkCopy.DestinationTableName = "[" + dt.TableName + "]"
			bulkCopy.WriteToServer(dt)
			bulkCopy.Close()

		Catch ex As Exception
			MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Import data to table failed")
			returnValue = False
		End Try

		Return returnValue
	End Function

	Private Function BuildCreateTableSQL(ByVal tableName As String, ByVal dt As DataTable) As String
		Dim SQL As String = "CREATE TABLE[" + tableName + "] ("

		'Columns
		For Each column As DataColumn In dt.Columns
			SQL += vbNewLine
			SQL += "[" + column.ColumnName + "] " + SQLGetType(column) + ","
		Next

		SQL = SQL.TrimEnd(","c)

		'Primary keys
		If dt.PrimaryKey.Length > 0 Then

			SQL += vbNewLine
			SQL += "CONSTRAINT [PK_" + tableName + "] PRIMARY KEY CLUSTERED ("

			For Each column As DataColumn In dt.PrimaryKey
				SQL += "[" + column.ColumnName + "],"
			Next

			SQL = SQL.TrimEnd(","c)
			SQL += ")"

		End If

		SQL += ")"

		Return SQL
	End Function

	Private Function SQLGetType(ByVal column As DataColumn) As String
		Dim str As String = ""

		Try
			Dim max As Integer = column.MaxLength
			If max > 8000 Then max = 8000
			str = SQLGetType(column.DataType, max)

		Catch ex As Exception
			MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Get SQL type 1 failed")
		End Try

		Return str
	End Function

	Private Function SQLGetType(ByVal type As Object, ByVal columnsize As Integer, Optional ByVal numericPrecision As Integer = 0, Optional ByVal numericScale As Integer = 0) As String

		Select Case type.ToString()
			Case "System.Boolean"
				Return "bit"
			Case "System.Byte"
				Return "tinyint"
			Case "System.Int16"
				Return "smallint"
			Case "System.Int32"
				Return "int"
			Case "System.Int64"
				Return "bigint"
			Case "System.Byte[]"
				If columnsize = -1 Or columnsize > 8000 Then
					Return "varbinary(max)"
				Else
					Return "varbinary(" & columnsize & ")"
				End If
			Case "System.Char[]"
				Return "char(" + columnsize + ")"
			Case "System.String"
				If columnsize = -1 Then
					Return "varchar(255)"
				ElseIf columnsize >= &H3FFFFFFF Then
					Return "text"
				Else
					Return "varchar(" & columnsize & ")"
				End If
			Case "System.Single"
				Return "real"
			Case "System.Double"
				Return "float"
			Case "System.Decimal"
				If numericPrecision > 0 Then
					If numericScale <= numericPrecision And numericScale >= 0 Then
						Return "decimal(" & numericPrecision & "," & numericScale & ")"
					Else
						Return "decimal(" & numericPrecision & ")"
					End If
				Else
					Return "decimal"
				End If
			Case "System.DateTime"
				Return "datetime"
			Case "System.Guid"
				Return "uniqueidentifier"
			Case "System.Object"
				Return "sql_variant"
			Case Else
				Throw New Exception(type.ToString() + " not implemented.")
		End Select
	End Function

	'Export to data set for ODBC connection
	Private Overloads Sub ExportToDataSet(ByVal srcDSN As String, ByRef ds As DataSet, ByRef errInfo As Exception)
		'Create the source ODBC connection object
		Dim odbcCon As OdbcConnection = New OdbcConnection(srcDSN)

		Try
			'Open a connection to the ODBC source
			odbcCon.Open()

			'Retrieve the Table schema info
			Dim tbls As DataTable = odbcCon.GetSchema("Tables")

			'For each user table, retrieve all the data and insert into our dataset
			For Each dr As DataRow In tbls.Rows

				Dim tableName As String = dr("TABLE_NAME")
				Dim tableType As String = dr("TABLE_TYPE")

				'Only get the data for the user tables (non-system)
				If (tableType = "TABLE") Then

					Dim ad As OdbcDataAdapter = New OdbcDataAdapter("SELECT * FROM [" + tableName + "]", odbcCon)

					Try
						'Add any schema info to the dataset first. This allows for proper key creation later
						ad.FillSchema(ds, SchemaType.Mapped, tableName)

						'Fill the dataset with the rows from the table
						ad.Fill(ds, tableName)

					Catch ex As OdbcException
						Debug.Print(ex.Message)
					Catch ex As Exception
						Debug.Print(ex.Message)
					End Try

				End If

			Next
			odbcCon.Close()

		Catch ex As OdbcException
			Debug.Print(ex.Message)

		Catch ex As Exception
			Debug.Print(ex.Message)

		Finally
			'Manually dispose of the odbc connection resource
			odbcCon.Dispose()
			odbcCon = Nothing
		End Try
	End Sub

	'Export to data set for LOCAL (SQL) connection
	Private Overloads Sub ExportToDataSet(ByRef ds As DataSet, ByRef errInfo As Exception)
		'Create the source LOCAL (SQL) connection object
		Dim sqlCon As SqlConnection = New SqlConnection(ucDUGONG.conString)

		Try
			'Open a connection to the LOCAL (SQL) source
			sqlCon.Open()
			'Retrieve the Table schema info
			Dim tbls As DataTable

			If tablesToInclude Is Nothing Then
				tbls = sqlCon.GetSchema("Tables")
			Else
				tbls = tablesToInclude
			End If

			'For each user table, retrieve all the data and insert into our dataset
			For Each dr As DataRow In tbls.Rows

				Dim tableName As String = dr("TABLE_NAME")
				Dim tableType As String = dr("TABLE_TYPE")


				'Only get the data for the user tables (non-system)
				If (tableType = "TABLE") Or (tableType = "BASE TABLE") Then

					Dim ad As SqlDataAdapter = New SqlDataAdapter("SELECT * FROM [" + tableName + "]", sqlCon)

					Try
						'Add any schema info to the dataset first. This allows for proper key creation later
						ad.FillSchema(ds, SchemaType.Mapped, tableName)

						'Fill the dataset with the rows from the table
						ad.Fill(ds, tableName)
					Catch : End Try
				End If
			Next

			sqlCon.Close()

		Catch ex As OdbcException
			errInfo = ex

		Catch ex As Exception
			errInfo = ex

		Finally
			'Manually dispose of the LOCAL (SQL) connection resource
			sqlCon.Dispose()
			sqlCon = Nothing
		End Try
	End Sub
End Class