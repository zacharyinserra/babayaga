﻿Imports System.Xml

Public Class clsSettingsManager

	Public Overloads Function GetSettingValue(ByVal SettingName As String, ByVal ConfigFile As String, Optional ByVal Decrypt As Boolean = False) As Object

		Dim oDoc As XmlDocument = New Xml.XmlDocument
		Dim oNodes As XmlNodeList
		Dim oNode As XmlNode
		Dim ConString As String = ""

		oDoc.Load(ConfigFile)

		oNodes = oDoc.GetElementsByTagName("setting")

		For Each oNode In oNodes
			Dim Attr As String = ""

			Attr = oNode.Attributes("name").Value

			If Attr.ToLower = SettingName.ToLower Then
				ConString = oNode.ChildNodes(0).InnerText

				If Decrypt = True Then
					ConString = SimpleEncryption.Decrypt(ConString)
				End If

				Exit For
			End If
		Next
		Return ConString
	End Function

End Class
