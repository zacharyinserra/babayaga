﻿Imports System.Text
Imports System.Data
Imports System.Web.Script.Serialization
Imports System.Runtime.InteropServices

Public Class clsTrialDetails
	Public Property Id As String
	Public Property ServerName As String
	Public Property InstallationDate As Date
	Public Property IsActivated As Boolean
	Public Property connectionString As String
	Public Property ActivationDate As Date
	Public Property ModifiedDate As Date
	Public Property connectionType As ConnectionTypeEnum

	Public Enum ConnectionTypeEnum
		NATIVE = 0
		ODBC = 1
		DAT = 2
	End Enum

	Public Sub New()
	End Sub

	Public Sub New(ByVal ConnectionString As String, ByVal _type As ConnectionTypeEnum, ByVal Optional createIfNotFound As Boolean = False, ByVal Optional cachedPackInfo As String = "")
		Me.connectionString = ConnectionString
		connectionType = _type
		createBaseTable()
		Dim sql = "SELECT [Id], [PackInfo], [ModifiedDate] FROM [PackVersionInfo]"
		Dim da As clsDataAccess = New clsDataAccess(ConnectionString, _type)
		Dim dt As DataTable = da.getRecords(sql)

		If dt.Rows.Count = 0 Then

			If createIfNotFound = False Then
				Throw New Exception("Error initiliazing trial details. Could not find trial or activation information")
			Else
				Dim err As Exception = Nothing
				Id = Guid.NewGuid.ToString
				InstallationDate = Date.Now
				ServerName = Environment.MachineName

				If RegisterInstallation(err, cachedPackInfo) = False Then
					Throw err
				End If
			End If
		Else
			Dim r = dt.Rows(0)
			Dim encodedString = CStr(r("packinfo"))
			Dim modifiedDate As Date = r("ModifiedDate")
			Dim td = GetTrialDetails(encodedString)
			ActivationDate = td.ActivationDate
			Id = td.Id
			InstallationDate = td.InstallationDate
			IsActivated = td.IsActivated
			ServerName = td.ServerName
			Me.ModifiedDate = modifiedDate
			dt.Dispose()
		End If
	End Sub

	Private Sub createBaseTable()
		Dim sql = "CREATE TABLE PackVersionInfo(
                            [Id] nvarchar(55) NOT NULL,
                            [EntryDate] datetime NULL,
                            [PackInfo] nvarchar(4000) NULL,
                            [ModifiedDate] datetime NULL)"

		If connectionType = ConnectionTypeEnum.DAT Then
			sql = "CREATE TABLE PackVersionInfo(
                            [Id] varchar(55) NOT NULL,
                            [EntryDate] datetime NULL,
                            [PackInfo] TEXT NULL,
                            [ModifiedDate] datetime NULL)"
		End If

		Dim da As clsDataAccess = New clsDataAccess(connectionString, connectionType)
		Dim parameters = New List(Of KeyValuePair(Of String, Object))

		Try
			da.executeQuery(sql, parameters)
		Catch e As Exception
			Console.WriteLine(e.ToString)
		End Try
	End Sub

	Public Function GetEncodedTrialInfo() As String
		Dim ser = New JavaScriptSerializer
		Dim JsonString = ser.Serialize(Me)
		Dim EncodedString = Convert.ToBase64String(Encoding.UTF8.GetBytes(JsonString))
		Return SimpleEncryption.clsEncryption.EncryptStringAES(EncodedString)
	End Function

	Public Shared Function GetTrialDetails(ByVal EncodedString As String) As clsTrialDetails
		EncodedString = SimpleEncryption.clsEncryption.DecryptStringAES(EncodedString)

		Try
			Dim ser = New System.Web.Script.Serialization.JavaScriptSerializer
			Dim decoded = Convert.FromBase64String(EncodedString)
			Dim trialDetails = ser.Deserialize(Of clsTrialDetails)(Encoding.UTF8.GetString(decoded))
			ser = Nothing
			Return trialDetails
		Catch e As Exception
			Console.WriteLine(e.ToString)
			Return Nothing
		End Try
	End Function

	Public Function ExtendTrial(<Out> ByRef errInfo As Exception) As Boolean
		InstallationDate = Date.Now
		Dim updatedInfo = GetEncodedTrialInfo()
		errInfo = Nothing

		Try
			Dim parameters = New List(Of KeyValuePair(Of String, Object))
			Dim sql = $"UPDATE [PackVersionInfo] SET [PackInfo] = '{updatedInfo.Replace("'", "''")}', [ModifiedDate] = GETDATE()"

			If connectionType = ConnectionTypeEnum.DAT Then
				sql = $"UPDATE [PackVersionInfo] SET [PackInfo] = '{updatedInfo.Replace("'", "''")}', [ModifiedDate] = NOW()"
			End If

			Dim da As clsDataAccess = New clsDataAccess(connectionString, connectionType)
			Return da.executeQuery(sql, parameters)
		Catch ex As Exception
			errInfo = ex
			Return False
		End Try
	End Function

	Public Function ActivateInstallation(<Out> ByRef errInfo As Exception) As Boolean
		IsActivated = True
		ActivationDate = Date.Now
		ModifiedDate = Date.Now
		Dim updatedInfo = GetEncodedTrialInfo()
		errInfo = Nothing

		Try
			Dim parameters = New List(Of KeyValuePair(Of String, Object))
			Dim sql = "UPDATE [PackVersionInfo] SET [PackInfo] = @packinfo, [ModifiedDate] = @modifiedDate"
			parameters.Add(New KeyValuePair(Of String, Object)("@packinfo", updatedInfo))
			parameters.Add(New KeyValuePair(Of String, Object)("@modifiedDate", Date.Now))

			If connectionType = ConnectionTypeEnum.DAT Then
				sql = $"UPDATE [PackVersionInfo] SET [PackInfo] = {updatedInfo.Replace("'", "''")}, [ModifiedDate] = Now()"
			End If

			Dim da As clsDataAccess = New clsDataAccess(connectionString, connectionType)
			Return da.executeQuery(sql, parameters)
		Catch ex As Exception
			errInfo = ex
			Return False
		End Try
	End Function

	Public Function RegisterInstallation(<Out> ByRef errInfo As Exception, ByVal Optional cachedPackInfo As String = "") As Boolean
		Try
			Dim trialInfo = ""

			If String.IsNullOrEmpty(cachedPackInfo) Then
				trialInfo = GetEncodedTrialInfo()
			Else
				trialInfo = cachedPackInfo
			End If

			Dim sql = "SELECT * FROM [PackVersionInfo]"
			errInfo = Nothing
			Dim da As clsDataAccess = New clsDataAccess(connectionString, connectionType)
			Dim dt As DataTable = da.getRecords(sql)

			If dt?.Rows.Count > 0 Then
				Return True
			Else
				Dim parameters = New List(Of KeyValuePair(Of String, Object))
				sql = $"INSERT INTO [PackVersionInfo] ([Id], [EntryDate], [PackInfo], [ModifiedDate]) VALUES(NEWID(), GETDATE(),'{trialInfo.Replace("'", "''")}', GETDATE())"

				If connectionType = ConnectionTypeEnum.DAT Then
					sql = $"INSERT INTO [PackVersionInfo] ([Id], [EntryDate], [PackInfo], [ModifiedDate]) VALUES('{Guid.NewGuid.ToString}', NOW(),'{trialInfo.Replace("'", "''")}', NOW())"
				End If

				da.executeQuery(sql, parameters)
			End If

			Return True
		Catch ex As Exception
			errInfo = ex
			Return False
		End Try
	End Function
End Class
