﻿Imports System.Text
Imports System.IO
Imports System.Security.Cryptography

Namespace SimpleEncryption
	Public Module clsEncryption
		Private ReadOnly _salt As Byte() = Encoding.UTF8.GetBytes("ABdUgsfXHUbZX9YcDCqonHRnWWuBscM9Dv}LCciarqedRejRgqfTyuZoekLLAsaD")
		Private ReadOnly sharedSecret As String = "fxMBP9CdPNqx}QZViBaX;CnF{GAshQ)y9igbDZYXjCBJdpjzfxQkAyqsUJHYmZha"
		Public mProvider As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider

		Sub New()
			mProvider = New TripleDESCryptoServiceProvider
			mProvider.Key = New Byte() {112, 223, 122, 83, 173, 22, 186, 153, 229, 183, 73, 133, 124, 124, 132, 13}
			mProvider.IV = New Byte() {173, 224, 14, 43, 253, 103, 82, 212}
		End Sub

		Function EncryptStringAES(ByVal plainText As String) As String
			If String.IsNullOrEmpty(plainText) Then Throw New ArgumentNullException("plainText")
			If String.IsNullOrEmpty(sharedSecret) Then Throw New ArgumentNullException("sharedSecret")
			Dim outStr As String = Nothing
			Dim aesAlg As RijndaelManaged = Nothing

			Try
				Dim key As Rfc2898DeriveBytes = New Rfc2898DeriveBytes(sharedSecret, _salt)
				aesAlg = New RijndaelManaged
				aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8)
				Dim encryptor As ICryptoTransform = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV)

				Using msEncrypt = New MemoryStream
					msEncrypt.Write(BitConverter.GetBytes(aesAlg.IV.Length), 0, 4)

					msEncrypt.Write(aesAlg.IV, 0, aesAlg.IV.Length)

					Using csEncrypt As CryptoStream = New CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write)

						Using swEncrypt = New StreamWriter(csEncrypt)
							swEncrypt.Write(plainText)
						End Using
					End Using

					outStr = Convert.ToBase64String(msEncrypt.ToArray)
				End Using
			Finally
				If aesAlg IsNot Nothing Then aesAlg.Clear()
			End Try

			Return outStr
		End Function

		Function DecryptStringAES(ByVal cipherText As String) As String
			If String.IsNullOrEmpty(cipherText) Then Throw New ArgumentNullException("cipherText")
			If String.IsNullOrEmpty(sharedSecret) Then Throw New ArgumentNullException("sharedSecret")
			Dim aesAlg As RijndaelManaged = Nothing
			Dim plaintext As String = Nothing

			Try
				Dim key As Rfc2898DeriveBytes = New Rfc2898DeriveBytes(sharedSecret, _salt)
				Dim bytes = Convert.FromBase64String(cipherText)

				Using msDecrypt = New MemoryStream(bytes)
					aesAlg = New RijndaelManaged
					aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8)
					aesAlg.IV = ReadByteArray(msDecrypt)
					Dim decryptor As ICryptoTransform = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV)

					Using csDecrypt As CryptoStream = New CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read)

						Using srDecrypt = New StreamReader(csDecrypt)
							plaintext = srDecrypt.ReadToEnd
						End Using
					End Using
				End Using

			Finally
				If aesAlg IsNot Nothing Then aesAlg.Clear()
			End Try

			Return plaintext
		End Function

		Private Function ReadByteArray(ByVal s As Stream) As Byte()
			Dim rawLength = New Byte(3) {}

			If s.Read(rawLength, 0, rawLength.Length) <> rawLength.Length Then
				Throw New SystemException("Stream did not contain properly formatted byte array")
			End If

			Dim buffer = New Byte(BitConverter.ToInt32(rawLength, 0) - 1) {}

			If s.Read(buffer, 0, buffer.Length) <> buffer.Length Then
				Throw New SystemException("Did not read byte array properly")
			End If

			Return buffer
		End Function

		Function Encrypt(ByVal AString As String) As String
			If AString = String.Empty Then
				Return AString
			Else
				Dim encryptedData() As Byte
				Dim dataStream As MemoryStream = Nothing

				Dim encryptor As ICryptoTransform
				encryptor = mProvider.CreateEncryptor()

				Try
					dataStream = New MemoryStream

					Dim encryptedStream As CryptoStream = Nothing
					Try
						'Create the encrypted stream
						encryptedStream = New CryptoStream(dataStream, encryptor, CryptoStreamMode.Write)

						Dim theWriter As StreamWriter = Nothing
						Try
							'Write the string to memory via the encryption algorithm
							theWriter = New StreamWriter(encryptedStream)
							'Write the string to the memory stream
							theWriter.Write(AString)

							'End the writing
							theWriter.Flush()
							encryptedStream.FlushFinalBlock()

							'Position back at start
							dataStream.Position = 0

							'Create area for data
							ReDim encryptedData(CInt(dataStream.Length))

							'Read data from memory
							dataStream.Read(encryptedData, 0, CInt(dataStream.Length))

							'Convert to String
							Return Convert.ToBase64String(encryptedData, 0, encryptedData.Length)
						Finally
							theWriter.Close()
						End Try
					Finally
						encryptedStream.Close()
					End Try
				Finally
					dataStream.Close()
				End Try
			End If
		End Function

		Function Decrypt(ByVal AString As String) As String
			Try
				If AString = String.Empty Then
					Return AString
				Else
					Dim encryptedData() As Byte
					Dim dataStream As MemoryStream = Nothing
					Dim encryptedStream As CryptoStream = Nothing
					Dim strLen As Integer

					'Get the byte data
					encryptedData = Convert.FromBase64String(AString)

					Try
						dataStream = New MemoryStream
						Try
							'Create decryptor and stream
							Dim decryptor As ICryptoTransform
							decryptor = mProvider.CreateDecryptor()
							encryptedStream = New CryptoStream(dataStream, decryptor, CryptoStreamMode.Write)

							'Write the decrypted data to the memory stream
							encryptedStream.Write(encryptedData, 0, encryptedData.Length - 1)
							encryptedStream.FlushFinalBlock()

							'Position back at start
							dataStream.Position = 0

							'Determine length of decrypted string
							strLen = CInt(dataStream.Length)

							'Create area for data
							ReDim encryptedData(strLen - 1)

							'Read decrypted data to byte()
							dataStream.Read(encryptedData, 0, strLen)

							'Construct string from byte()
							Dim retStr As String = ""

							Dim i As Integer
							For i = 0 To strLen - 1
								retStr += Chr(encryptedData(i))
							Next

							'Return result
							Return retStr
						Finally
							encryptedStream.Close()
						End Try
					Finally
						dataStream.Close()
					End Try
				End If
			Catch ex As Exception
				Return AString
			End Try
		End Function

	End Module
End Namespace
