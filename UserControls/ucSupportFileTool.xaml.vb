﻿Imports System.ComponentModel
Imports Microsoft.Win32

Public Class ucSupportFileTool

	Public Shared vmSuppFileTool As viewModelucSupportFileTool = New viewModelucSupportFileTool
	Private WithEvents worker As BackgroundWorker = New BackgroundWorker

	Public Sub New()

		InitializeComponent()
		Me.DataContext = vmSuppFileTool

	End Sub

	Private Sub btnBrowseDefFile_Click(sender As Object, e As RoutedEventArgs) Handles btnBrowseDefFile.Click
		Dim ofd As OpenFileDialog = New OpenFileDialog()
		ofd.Filter = "DEF Files (*.def)|*.def|All files|*.*"
		If ofd.ShowDialog() = True Then
			vmSuppFileTool.propDefFileLocation = ofd.FileName
		End If
	End Sub

	Private Sub btnConfirm_Click(sender As Object, e As RoutedEventArgs) Handles btnConfirm.Click
		btnConfirm.IsEnabled = False
		worker.RunWorkerAsync()
	End Sub

	Private Sub BeginImport()
		vmSuppFileTool.propProgress = 10

		'Check that necessary information is filled out
		If vmSuppFileTool.propDefFileLocation.Length = 0 Then
			MsgBox("Please specify DEF file location", MsgBoxStyle.Critical, "Error")
			txtDefFileLocation.Focus()
			Exit Sub
		ElseIf vmSuppFileTool.propServerName.Length = 0 Then
			MsgBox("Please specify server name", MsgBoxStyle.Critical, "Error")
			txtServerName.Focus()
			Exit Sub
		ElseIf vmSuppFileTool.propDatabaseName.Length = 0 Then
			MsgBox("Please specify database name", MsgBoxStyle.Critical, "Error")
			txtDatabaseName.Focus()
			Exit Sub
		ElseIf Not vmSuppFileTool.propUseWindowsAuth Then
            If vmSuppFileTool.propUsername.Length = 0 Then
                MsgBox("Please specify username", MsgBoxStyle.Critical, "Error")
                txtUsername.Focus()
                Exit Sub
            ElseIf txtPassword.password.length = 0 Then
                MsgBox("Please specify password", MsgBoxStyle.Critical, "Error")
                txtPassword.Focus()
				Exit Sub
			End If
		End If

		'Build DSN destination string
		Dim destDSN As String = $"Server={vmSuppFileTool.propServerName};Database={vmSuppFileTool.propDatabaseName};"
		If vmSuppFileTool.propUseWindowsAuth Then
			destDSN += "Trusted_Connection=True;"
		Else
			destDSN += $"User ID={vmSuppFileTool.propUsername};"
            destDSN += $"Password={txtPassword.Password};"
            destDSN += "Trusted_Connection=False;"
		End If

		'Save settings
		Try
			My.Settings.DefFileLocation = vmSuppFileTool.propDefFileLocation
			My.Settings.ServerName = vmSuppFileTool.propServerName
			My.Settings.DatabaseName = vmSuppFileTool.propDatabaseName
			My.Settings.UseWindowsAuth = vmSuppFileTool.propUseWindowsAuth
			If vmSuppFileTool.propUseWindowsAuth Then
				My.Settings.WindowsUsername = vmSuppFileTool.propUsername
                My.Settings.WindowsPassword = txtPassword.Password
            End If
			My.Settings.Save()
		Catch : End Try

		'Begin import
		Dim dataMigrator As clsDataMigrator = New clsDataMigrator
        Dim chk As Boolean = dataMigrator.Import(destDSN, vmSuppFileTool.propDefFileLocation)
        If chk Then
            MsgBox("Import successful", MsgBoxStyle.Information, "Success")
        Else
            MsgBox("Import failed", MsgBoxStyle.Exclamation, "Failure")
        End If
    End Sub

	Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
		Window.GetWindow(Me).Close()
	End Sub

	Private Sub chkUseWindowsAuth_Checked(sender As Object, e As RoutedEventArgs) Handles chkUseWindowsAuth.Checked
		txtUsername.IsEnabled = False
		txtPassword.IsEnabled = False
	End Sub

	Private Sub chkUseWindowsAuth_Unchecked(sender As Object, e As RoutedEventArgs) Handles chkUseWindowsAuth.Unchecked
		txtUsername.IsEnabled = True
		txtPassword.IsEnabled = True
	End Sub

	Private Sub worker_DoWork(sender As Object, e As DoWorkEventArgs) Handles worker.DoWork
		BeginImport()
	End Sub

	Private Sub worker_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles worker.RunWorkerCompleted
		vmSuppFileTool.propProgress = 100
		worker.Dispose()
		btnConfirm.IsEnabled = True
	End Sub
End Class
