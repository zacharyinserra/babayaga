﻿Imports System.Windows.Forms

Public Class ucSiana

	Private vmSaina As viewModelucSaina = New viewModelucSaina
	Private Const signTool As String = "C:\CSS\babayaga\bin\signtool.exe"

	Public Sub New()

		InitializeComponent()
		Me.DataContext = vmSaina
		lbFiles.ItemsSource = vmSaina.propFiles

	End Sub
	Private Sub btnAddFiles_Click(sender As Object, e As RoutedEventArgs) Handles btnAddFiles.Click

		Dim ofd As OpenFileDialog = New OpenFileDialog
		ofd.Multiselect = True

		If ofd.ShowDialog = DialogResult.Cancel Then Return

		For Each file As String In ofd.FileNames
			vmSaina.propFiles.Add(file)
			lbFiles.Items.Refresh()
		Next

	End Sub

	Private Sub btnRemoveFile_Click(sender As Object, e As RoutedEventArgs) Handles btnRemoveFile.Click
		vmSaina.propFiles.Remove(lbFiles.SelectedItems.ToString)
	End Sub

	Private Sub btnSign_Click(sender As Object, e As RoutedEventArgs) Handles btnSign.Click
		For Each file In vmSaina.propFiles
			Dim signArgs As String = $"sign /debug /t http://timestamp.verisign.com/scripts/timstamp.dll /fd sha256 /f ""\\publisher\css\csssaina\Saina\bin\Release\cert.p12"" /p sar1532nika ""{file}"""
			Dim process As Process = New Process
			process.StartInfo.FileName = signTool
			process.StartInfo.Arguments = signArgs
			process.Start()
			process.WaitForExit()
		Next

		MessageBox.Show("File(s) signed!")
	End Sub

	Private Sub btnExit_Click(sender As Object, e As RoutedEventArgs) Handles btnExit.Click
		Window.GetWindow(Me).Close()
	End Sub
End Class
