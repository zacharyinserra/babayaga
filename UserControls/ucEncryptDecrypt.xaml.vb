﻿Public Class ucEncryptDecrypt

	Public Shared vmHyptoCrypto As viewModelucEncryptDecrypt = New viewModelucEncryptDecrypt

	Public Sub New()

		InitializeComponent()
		Me.DataContext = vmHyptoCrypto

	End Sub

	Private Sub btnEncrypt_Click(sender As Object, e As RoutedEventArgs) Handles btnEncrypt.Click
		vmHyptoCrypto.propEncryptOutput = SimpleEncryption.Encrypt(vmHyptoCrypto.propEncryptInput)
	End Sub

	Private Sub btnDecrypt_Click(sender As Object, e As RoutedEventArgs) Handles btnDecrypt.Click
		vmHyptoCrypto.propDecryptOutput = SimpleEncryption.Decrypt(vmHyptoCrypto.propDecryptInput)
	End Sub

	Private Sub btnExit_Click(sender As Object, e As RoutedEventArgs) Handles btnExit.Click
		Window.GetWindow(Me).Close()
	End Sub
End Class
