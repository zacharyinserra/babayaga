﻿Public Class ucExtendo

	Private ReadOnly vmExtendo As viewModelucExtendo = New viewModelucExtendo

	Public Sub New()

		InitializeComponent()
		Me.DataContext = vmExtendo

	End Sub
	Private Async Sub btnConfirm_Click(sender As Object, e As RoutedEventArgs) Handles btnConfirm.Click
		vmExtendo.propProg = 0
		If vmExtendo.propServerName = String.Empty Then
			MessageBox.Show("Please enter a server name")
		End If

		btnConfirm.IsEnabled = False
		btnConfirm.Content = "Please wait..."

		Dim trialDetails As clsTrialDetails = New clsTrialDetails

		trialDetails.ServerName = vmExtendo.propServerName
		trialDetails.ModifiedDate = DateTime.Now

		Try
			trialDetails.IsActivated = False
			trialDetails.InstallationDate = DateTime.Now
		Catch : End Try

		Await Animate()

		vmExtendo.propEncodedInfo = trialDetails.GetEncodedTrialInfo

		btnConfirm.Content = "Confirm"
		btnConfirm.IsEnabled = True
	End Sub

	Private Async Function Animate() As Task
		Await Task.Run(Sub()
						   While vmExtendo.propProg < 100
							   If Not Dispatcher.CheckAccess() Then
								   Dispatcher.Invoke(Sub()
														 vmExtendo.propProg += CInt(Math.Ceiling(Rnd() * 5)) + 1
													 End Sub)
							   Else
								   vmExtendo.propProg += CInt(Math.Ceiling(Rnd() * 5)) + 1
							   End If
							   Threading.Thread.Sleep(30)
						   End While
					   End Sub)
	End Function

	Private Sub btnCopyCode_Click(sender As Object, e As RoutedEventArgs) Handles btnCopyCode.Click
		If vmExtendo.propEncodedInfo <> String.Empty Then
			Clipboard.SetText(vmExtendo.propEncodedInfo)
			vmExtendo.propStatus = "Code copied to clipboard!"
		End If
	End Sub

	Private Sub btnExit_Click(sender As Object, e As RoutedEventArgs) Handles btnExit.Click
		Window.GetWindow(Me).Close()
	End Sub

End Class
