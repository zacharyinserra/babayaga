﻿Imports System.ComponentModel
Imports System.Data.Odbc
Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class ucDUGONG

	Private vmDouglas As viewModelucDUGONG = New viewModelucDUGONG
	Private WithEvents worker As BackgroundWorker = New BackgroundWorker

	Public Shared conString As String
	Public conType As String
	Public custNo As String
	Public product As String
	Public dType

#If DEBUG Then
	Public appPath As String = "C:\Program Files (x86)\ChristianSteven"
#Else
	Public appPath As String = IO.Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory)
#End If

	Public Sub New()

		InitializeComponent()
		Me.DataContext = vmDouglas

		gridConnect.Visibility = Visibility.Visible
		gridQueryBuilder.Visibility = Visibility.Hidden
		gridReview.Visibility = Visibility.Hidden

		rbOverwrite.IsChecked = True

	End Sub

	Private Sub btnBrowse_Click(sender As Object, e As RoutedEventArgs) Handles btnBrowse.Click
		Dim fbd As FolderBrowserDialog = New FolderBrowserDialog()
		If fbd.ShowDialog() = DialogResult.OK Then
			vmDouglas.propBackupDest = fbd.SelectedPath
		End If
	End Sub

	Private Sub btnConnect_Click(sender As Object, e As RoutedEventArgs) Handles btnConnect.Click
		worker.RunWorkerAsync()
	End Sub
	'
	'GridConnect
	'
	Private Sub BeginDugonging()
		vmDouglas.propProg = 10
		If vmDouglas.propProduct = "" Then
			MessageBox.Show("Please select a product.")
			Return
		ElseIf IO.Directory.Exists(vmDouglas.propBackupDest) = False Then
			MessageBox.Show("Please select a valid folder path.")
			Return
		Else
			Try
				My.Settings.Product = vmDouglas.propProduct
				My.Settings.BackupDest = vmDouglas.propBackupDest

				product = vmDouglas.propProduct
				Dim settingMan As clsSettingsManager = New clsSettingsManager
				Dim configPath As String

				vmDouglas.propProg = 20

				configPath = IO.Path.Combine(appPath, product, product.ToLower & "live.config")
				'Use settingMan to get conString from live.config file
				conType = settingMan.GetSettingValue("ConType", configPath, False)
				vmDouglas.propProg = 30
				conString = settingMan.GetSettingValue("ConString", configPath, True)
				vmDouglas.propProg = 40
				custNo = settingMan.GetSettingValue("custNo", configPath, False)
				vmDouglas.propProg = 50
				conString = localConStringdotNET()

				GenerateBackup()
			Catch Ex As Exception
				MessageBox.Show("Something Failed" & Ex.Message)
			End Try
		End If
	End Sub

	Private Function localConStringdotNET() As String
		Try
			'process constring
			Dim dataSource, catalog, userid, password, persist, integratedsecurity As String

			'For non-standard
			'using Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=SQL-RD;Data Source=FDCA01
			'for non standard using NT auth Data Source=AUTOSERVER1;Initial Catalog=crd;Integrated Security=True
			If conString <> "" Then
				For Each s As String In conString.Split(";")
					Dim key, value As String

					key = s.Split("=")(0)
					value = s.Split("=")(1)

					Select Case key.ToLower
						Case "initial catalog"
							catalog = value
						Case "data source"
							dataSource = value
						Case "password"
							password = value
						Case "user id"
							userid = value
						Case "persist security info"
							persist = value
						Case "integrated security"
							integratedsecurity = value
					End Select
				Next

				Dim newCon As String = "Data Source=" & dataSource & ";Initial Catalog=" & catalog

				If persist <> "" Then newCon &= ";Persist Security Info=" & persist
				If userid <> "" Then newCon &= ";User ID=" & userid
				If password <> "" Then newCon &= ";Password=" & password
				If integratedsecurity <> "" Then newCon &= ";Integrated Security=" & integratedsecurity

				Return newCon
			Else
				Return "Data Source=" & Environment.MachineName & "\CRD;Initial Catalog=CRD;Persist Security Info=True;User ID=sa;Password=cssAdmin12345$"
			End If
		Catch
			Return "Data Source=" & Environment.MachineName & "\CRD;Initial Catalog=CRD;Persist Security Info=True;User ID=sa;Password=cssAdmin12345$"
		End Try
	End Function

	Private Sub GenerateBackup()
		'Generates a backup of the DB before any changes can be made
		Dim srcDSN As String
		Dim dsn As String
		Dim user As String
		Dim pwd As String
		Dim ok As Boolean
		vmDouglas.propProg = 60
		If conType = "LOCAL" Then

			Dim dm As New clsDataMigrator(clsDataMigrator.dbType.LOCAL)
			Dim errInfo As Exception = Nothing

			ok = dm.Export("", appPath & "\" & product & "\" & product.ToLower & "live.def", errInfo)

			If ok = False Then
				If errInfo Is Nothing Then
					Throw New Exception("Could not create def file")
				Else
					Throw errInfo
				End If
			End If
		ElseIf conType = "ODBC" Then

			If conString.Length > 0 Then
				dsn = conString.Split(";")(4).Split("=")(1)
				user = conString.Split(";")(3).Split("=")(1)
				pwd = conString.Split(";")(1).Split("=")(1)
				srcDSN = "DSN=" & dsn & ";Uid=" & user & ";Pwd=" & pwd & ";"
			End If

			Dim dm As New clsDataMigrator(clsDataMigrator.dbType.ODBC)
			Dim errInfo As Exception = Nothing

			ok = dm.Export(conString, appPath & "\" & product & "\" & product.ToLower & "live.def", errInfo)

			If ok = False Then
				If errInfo Is Nothing Then
					Throw New Exception("Could not create def file")
				Else
					Throw errInfo
				End If
			End If

		Else
			ok = True
		End If
		vmDouglas.propProg = 70

		Try
			Dim FilezToZip As String
			Dim ZipFile As String = product & "_" & custNo & "_" & Date.Now.ToString("yyyyMMdd")

			Try
				FilezToZip = appPath & "\" & product & "\" & product.ToLower & "live.def"
			Catch ex As Exception
				MessageBox.Show("live.def file not found.")
			End Try
			vmDouglas.propProg = 80
			ZipFiles(vmDouglas.propBackupDest & "\" & ZipFile, FilezToZip)
			vmDouglas.propProg = 90
		Catch ex As Exception
		End Try
	End Sub

	Public Sub ZipFiles(ByRef path As String, ByRef filesToZip As String)
		While IO.Directory.Exists(path)
			path &= 0
		End While
		IO.Directory.CreateDirectory(path)
		IO.File.Copy(filesToZip, path & "\" & product.ToLower & "live.def")
	End Sub
	'
	'GridQueryBuilder
	'
	Private Sub fillTables()
		Dim SQL As String = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES"
		Dim reader = Nothing
		Dim con = Nothing
		If conType = "LOCAL" Then

			con = New SqlConnection(conString)
			con.Open()
			Dim sqlComm As SqlCommand = New SqlCommand(SQL, con)
			reader = sqlComm.ExecuteReader

		Else 'ConType = "ODBC"

			Dim cn As OdbcConnection = New OdbcConnection(conString)
			con.Open()
			Dim odbcComm As OdbcCommand = New OdbcCommand(SQL, con)
			reader = odbcComm.ExecuteReader

		End If

		While reader.Read()
			vmDouglas.propTables.Add(reader.GetValue(0))
		End While
		vmDouglas.propTables.Sort()
		con.Close()
		con.Dispose()
		con = Nothing
	End Sub

	Private Sub cmbTable_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles cmbTable.SelectionChanged
		cmbColumn.Items.Clear()
		cmbWhereColumn.Items.Clear()
		Dim SQL As String = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME LIKE '" & cmbTable.SelectedItem & "'"
		Dim reader = Nothing
		Dim con = Nothing
		Try
			If conType = "LOCAL" Then
				con = New SqlConnection(conString)
				con.Open()
				Dim sqlComm As SqlCommand = New SqlCommand(SQL, con)
				reader = sqlComm.ExecuteReader
			Else 'ConType = "ODBC"
				con = New SqlConnection(conString)
				con.Open()
				Dim sqlComm As SqlCommand = New SqlCommand(SQL, con)
				reader = sqlComm.ExecuteReader
			End If
		Catch
			Exit Sub
		End Try
		While reader.Read()
			cmbColumn.Items.Add(reader.GetValue(0))
			cmbWhereColumn.Items.Add(reader.getValue(0))
		End While
		con.Close()
		con.Dispose()
		con = Nothing
	End Sub

	Private Sub cmbColumn_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles cmbColumn.SelectionChanged
		Dim Sql As String = "SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME LIKE '" & cmbColumn.SelectedItem & "' AND TABLE_NAME LIKE '" & cmbTable.SelectedItem & "'"
		Dim reader = Nothing
		Dim con = Nothing
		Try
			If conType = "LOCAL" Then
				con = New SqlConnection(conString)
				con.Open()
				Dim sqlComm As SqlCommand = New SqlCommand(Sql, con)
				reader = sqlComm.ExecuteReader
			Else 'ConType = "ODBC"
				con = New SqlConnection(conString)
				con.Open()
				Dim sqlComm As SqlCommand = New SqlCommand(Sql, con)
				reader = sqlComm.ExecuteReader
			End If
		Catch
			Exit Sub
		End Try
		While reader.Read()
			dType = reader.GetValue(0)
		End While
		con.Close()
		con.Dispose()
		con = Nothing
	End Sub

	Private Sub cmbWhereColumn_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles cmbWhereColumn.SelectionChanged
		cmbWhereValue.Items.Clear()
		Dim SQL As String = "SELECT DISTINCT CAST(" & cmbWhereColumn.SelectedItem & " AS VARCHAR(MAX)) FROM " & cmbTable.SelectedItem
		Dim reader = Nothing
		Dim con = Nothing
		Try
			If conType = "LOCAL" Then
				con = New SqlConnection(conString)
				con.Open()
				Dim sqlComm As SqlCommand = New SqlCommand(SQL, con)
				reader = sqlComm.ExecuteReader
			Else 'ConType = "ODBC"
				con = New SqlConnection(conString)
				con.Open()
				Dim sqlComm As SqlCommand = New SqlCommand(SQL, con)
				reader = sqlComm.ExecuteReader
			End If
		Catch
			Exit Sub
		End Try
		While reader.Read()
			If Not IsDBNull(reader.GetValue(0)) AndAlso reader.getValue(0) <> "" Then
				cmbWhereValue.Items.Add(reader.getValue(0))
			End If
		End While
		con.Close()
		con.Dispose()
		con = Nothing
	End Sub

	Private Sub btnVerify_Click(sender As Object, e As EventArgs) Handles btnVerify.Click
		Dim EncryptVal = ""
		vmDouglas.propQuery = ""
		Dim qS As New clsQueryBuilder

		If chkEncrypt.IsChecked Then
			Dim settingMan As New clsSettingsManager
			EncryptVal = "$$" + SimpleEncryption.Encrypt(txtInsert.Text) + "$$"
		End If

		If rbOverwrite.IsChecked Then
			If cmbTable.Text = "" Or cmbColumn.Text = "" Or txtInsert.Text = "" Then
				MessageBox.Show("Please fill out all value boxes before viewing your final query.", "Bulk Update", MessageBoxButtons.OK, MessageBoxIcon.Warning)
				Return
			Else
				Dim OpWhere
				Select Case cmbOperator.Text.ToLower
					Case "begins with"
						OpWhere = " LIKE '" & cmbWhereValue.Text & "%'"
					Case "ends with"
						OpWhere = " LIKE '%" & cmbWhereValue.Text & "'"
					Case "contains"
						OpWhere = " LIKE '%" & cmbWhereValue.Text & "%'"
					Case "does not contain"
						OpWhere = " NOT LIKE '%" & cmbWhereValue.Text & "%'"
					Case "does not begin with"
						OpWhere = " NOT LIKE '" & cmbWhereValue.Text & "%'"
					Case "does not end with"
						OpWhere = " NOT LIKE '%" & cmbWhereValue.Text & "'"
					Case "is empty"
						OpWhere = " IS NULL OR " &
						cmbTable.Text & "." & cmbWhereColumn.Text & " ='')"
					Case "is not empty"
						OpWhere = " IS NOT NULL AND " &
						cmbTable.Text & "." & cmbWhereColumn.Text & " <> '')"
					Case Else
						OpWhere = cmbOperator.Text & " '" & cmbWhereValue.Text & "'"
				End Select
				If EncryptVal <> "" Then
					qS.AppendString("Update " & cmbTable.Text & " Set " & cmbColumn.Text & " = '" & EncryptVal & "'")
				Else
					qS.AppendString("Update " & cmbTable.Text & " Set " & cmbColumn.Text & " = '" & txtInsert.Text & "'")
				End If

				If cmbWhereColumn.Text <> "" And cmbOperator.Text <> "" Then
					qS.AppendString(" Where " & cmbWhereColumn.Text & " " & OpWhere)
				End If
			End If
		ElseIf rbReplace.IsChecked Then
			If cmbTable.Text = "" Or cmbColumn.Text = "" Or txtInsert.Text = "" Or txtInsert.Text = "" Then
				MessageBox.Show("Please fill out all values boxes before viewing your final query.", "Bulk Update", MessageBoxButtons.OK, MessageBoxIcon.Warning)
				Return
			Else
				Dim OpWhere
				Select Case cmbOperator.Text.ToLower
					Case "begins with"
						OpWhere = " LIKE '" & cmbWhereValue.Text & "%'"
					Case "ends with"
						OpWhere = " LIKE '%" & cmbWhereValue.Text & "'"
					Case "contains"
						OpWhere = " LIKE '%" & cmbWhereValue.Text & "%'"
					Case "does not contain"
						OpWhere = " NOT LIKE '%" & cmbWhereValue.Text & "%'"
					Case "does not begin with"
						OpWhere = " NOT LIKE '" & cmbWhereValue.Text & "%'"
					Case "does not end with"
						OpWhere = " NOT LIKE '%" & cmbWhereValue.Text & "'"
					Case "is empty"
						OpWhere = " IS NULL OR " &
						cmbTable.Text & "." & cmbWhereColumn.Text & " ='')"
					Case "is not empty"
						OpWhere = " IS NOT NULL AND " &
						cmbTable.Text & "." & cmbWhereColumn.Text & " <> '')"
					Case Else
						OpWhere = cmbOperator.Text & " '" & cmbWhereValue.Text & "'"
				End Select
				If dType.ToLower = "text" Then
					qS.AppendString("Update " & cmbTable.Text & " Set " & cmbColumn.Text & " = Replace(CAST(" & cmbColumn.Text & " AS VARCHAR(MAX)), '" & txtReplacedValue.Text & "', '" & txtInsert.Text & "')")
				Else
					qS.AppendString("Update " & cmbTable.Text & " Set " & cmbColumn.Text & " = Replace(" & cmbColumn.Text & ", '" & txtReplacedValue.Text & "', '" & txtInsert.Text & "')")
				End If
				If cmbWhereColumn.Text <> "" And cmbOperator.Text <> "" Then
					qS.AppendString(" Where " & cmbWhereColumn.Text & " " & OpWhere)
				End If
			End If
		End If
		vmDouglas.propQuery = qS.ReturnString()
		qS.ClearString()
		gridQueryBuilder.Visibility = Visibility.Hidden
		gridReview.Visibility = Visibility.Visible
	End Sub

	Private Sub rbReplace_Checked(sender As Object, e As RoutedEventArgs) Handles rbReplace.Checked
		chkEncrypt.IsChecked = False
		chkEncrypt.IsEnabled = False
		txtReplacedValue.IsEnabled = True
	End Sub

	Private Sub rbOverwrite_Checked(sender As Object, e As RoutedEventArgs) Handles rbOverwrite.Checked
		chkEncrypt.IsEnabled = True
		txtReplacedValue.IsEnabled = False
	End Sub
	'
	'GridReview
	'
	Private Sub btnBack_Click(sender As Object, e As RoutedEventArgs) Handles btnBack.Click
		gridReview.Visibility = Visibility.Hidden
		gridQueryBuilder.Visibility = Visibility.Visible
		btnExecute.IsEnabled = False
	End Sub

	Private Sub btnTest_Click(sender As Object, e As RoutedEventArgs) Handles btnTest.Click
		Dim con As SqlConnection = New SqlConnection(conString)
		con.Open()
		Dim NoExec As String = "SET NOEXEC ON; " & txtQuery.Text & "; SET NOEXEC OFF;"
		Dim cmd As SqlCommand = New SqlCommand(NoExec, con)
		Try
			Dim numofRows = cmd.ExecuteNonQuery()
			MessageBox.Show("Query returned valid! Ready for Execution!", "Bulk Update", MessageBoxButtons.OK, MessageBoxIcon.Information)
			btnExecute.IsEnabled = True
		Catch ex As Exception
			MessageBox.Show("Query returned invalid, please check syntax and parameters!", "Bulk Update", MessageBoxButtons.OK, MessageBoxIcon.Warning)
		End Try
	End Sub

	Private Sub btnExecute_Click(sender As Object, e As EventArgs) Handles btnExecute.Click
		Dim con As SqlConnection = New SqlConnection(conString)
		con.Open()
		Dim cmd As SqlCommand = New SqlCommand(txtQuery.Text, con)
		Try
			Dim numofRows = cmd.ExecuteNonQuery()
			MessageBox.Show("Query Execution Complete, the table has been updated! Number of Rows that may have been affected: " & numofRows, "Bulk Update", MessageBoxButtons.OK, MessageBoxIcon.Information)
			txtQuery.Clear()
			btnExecute.IsEnabled = False
			gridReview.Visibility = Visibility.Hidden
			gridQueryBuilder.Visibility = Visibility.Visible
		Catch ex As Exception
			MessageBox.Show("Query Execution Failed!", "Bulk Update", MessageBoxButtons.OK, MessageBoxIcon.Warning)
		End Try
	End Sub

	Private Sub rbOverwrite_MouseEnter(sender As Object, e As EventArgs) Handles rbOverwrite.MouseEnter
		txtInfo.Text = "Overwrite will replace an entire column value"
		txtInfo.Visibility = Visibility.Visible
	End Sub

	Private Sub rbReplace_MouseEnter(sender As Object, e As EventArgs) Handles rbReplace.MouseEnter
		txtInfo.Text = "Replace changes part of a column value"
		txtInfo.Visibility = Visibility.Visible
	End Sub

	Private Sub rbReplace_MouseLeave(sender As Object, e As EventArgs) Handles rbReplace.MouseLeave, rbOverwrite.MouseLeave
		txtInfo.Visibility = Visibility.Hidden
		txtInfo.Text = ""
	End Sub

	Private Sub btnExit_Click(sender As Object, e As RoutedEventArgs) Handles btnExit.Click
		Window.GetWindow(Me).Close()
	End Sub

	Private Sub worker_DoWork(sender As Object, e As DoWorkEventArgs) Handles worker.DoWork
		BeginDugonging()
	End Sub

	Private Sub worker_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles worker.RunWorkerCompleted
		vmDouglas.propProg = 100
		MessageBox.Show("Backup generated successfully." & Environment.NewLine & "Database connection established.")
		worker.Dispose()

		If gridConnect.Visibility = Visibility.Visible Then
			gridConnect.Visibility = Visibility.Hidden
			gridQueryBuilder.Visibility = Visibility.Visible
		End If

		fillTables()
	End Sub
End Class
