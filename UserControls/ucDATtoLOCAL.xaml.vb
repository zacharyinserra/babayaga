﻿Imports System.ComponentModel
Imports System.Windows.Forms

Public Class ucDATtoLOCAL

    Public Shared vmDAT2LOCAL As viewModelucDATtoLOCAL = New viewModelucDATtoLOCAL
    Private WithEvents worker As BackgroundWorker = New BackgroundWorker

    Public datFile As String
    Public server As String
    Public database As String
    Public username As String
    Public password As String
    Public conStringDAT As String
    Public conStringLOCAL As String

    Public Sub New()

        InitializeComponent()
        Me.DataContext = vmDAT2LOCAL

    End Sub

    Private Function CopySystemData(ByVal userName As String, ByVal password As String, ByVal conStringDAT As String, ByVal conStringLOCAL As String, Optional ByVal exceptTables As ArrayList = Nothing) As Boolean

        Try
            vmDAT2LOCAL.propProgress = 10

            Dim tables As New List(Of String)
            Dim values As String

            GetTables(tables, conStringDAT)

            Dim connDAT As ADODB.Connection = New ADODB.Connection
            Dim connLOCAL As ADODB.Connection = New ADODB.Connection

            connLOCAL.Open(conStringLOCAL)
            connDAT.Open(conStringDAT)

            vmDAT2LOCAL.propProgress = 15
            Dim increment = 60 / tables.Count

            For Each table As String In tables

                vmDAT2LOCAL.propProgress += increment

                Dim SQL As String = "SELECT * FROM " & "[" & table & "]"
                Dim oRs As ADODB.Recordset = New ADODB.Recordset
                Try
                    oRs = connDAT.Execute(SQL)
                Catch ex As Exception
                    Continue For
                End Try
                Dim columns As String = ""
                Dim curRecord As Integer = 1

                For Each fld As ADODB.Field In oRs.Fields
                    columns &= "[" & fld.Name & "],"
                Next

                columns = columns.Substring(0, columns.Length - 1)

                Do While oRs.EOF = False

                    values = String.Empty

                    For Each s As String In columns.Split(",")
                        If s.Length > 0 Then
                            Dim field As String = s.Replace("[", "").Replace("]", "")
                            Dim query As String = "SELECT * FROM " & table
                            values &= ConvertToFieldType(query, connDAT, field, oRs(field).Value) & ","
                        End If
                    Next

                    values = values.Substring(0, values.Length - 1)

                    SQL = "INSERT INTO " & table & " (" & columns & ") VALUES (" & values & ")"

                    Try
                        connLOCAL.Execute(SQL)
                    Catch ex As Exception
                    End Try

                    oRs.MoveNext()

                    curRecord += 1

                Loop

                oRs.Close()
            Next

            connDAT.Close()
            connLOCAL.Close()
            connDAT = Nothing
            connLOCAL = Nothing

            Return True
        Catch
            Return False
        End Try
    End Function

    Private Sub GetTables(ByVal tables As List(Of String), ByVal conStringDAT As String)

        Dim tab As New ADOX.Table
        Dim col As New ADOX.Column
        Dim cat As New ADOX.Catalog
        Dim conn As New ADODB.Connection
        Dim tName As String
        Dim database As String
        Dim showSystem As Boolean = False

        tables.Clear()

        With conn
            .Open(conStringDAT)

            Try
                database = .ConnectionString
            Catch ex As Exception
                database = String.Empty
            End Try
        End With

        If database.ToLower.IndexOf("excel") > -1 Then
            showSystem = True
        End If

        cat.ActiveConnection = conn

        For Each tb In cat.Tables
            If showSystem = True Then
                If tb.Type = "TABLE" Or tb.Type = "SYSTEM TABLE" Then
                    If InStr(1, tb.Name, " ") > 0 Or InStr(1, tb.Name, "$") > 0 Or InStr(1, tb.Name, "-") > 0 Then
                        tName = "[" & tb.Name & "]"
                    Else
                        tName = tb.Name
                    End If

                    tables.Add(tName)
                End If
            Else

                If tb.Type = "TABLE" Then
                    If InStr(1, tb.Name, " ") > 0 Or InStr(1, tb.Name, "$") > 0 Or InStr(1, tb.Name, "-") > 0 Then
                        tName = "[" & tb.Name & "]"
                    Else
                        tName = tb.Name
                    End If

                    tables.Add(tName)
                End If
            End If
        Next

        tab = Nothing
        col = Nothing
        cat = Nothing
        conn = Nothing

    End Sub

    Private Sub BtnExit_Click(sender As Object, e As RoutedEventArgs) Handles btnExit.Click
        Window.GetWindow(Me).Close()
    End Sub

    Private Sub BtnMigrate_Click(sender As Object, e As RoutedEventArgs) Handles btnMigrate.Click

        datFile = txtDATFile.Text
        server = txtServer.Text
        database = txtDatabase.Text
        username = txtUsername.Text
        password = txtPassword.Password

        btnMigrate.IsEnabled = False
        worker.RunWorkerAsync()
    End Sub

    Private Sub btnBrowse_Click(sender As Object, e As RoutedEventArgs)
        Dim ofd As New OpenFileDialog
        ofd.Filter = "DAT File (*.dat) | *.dat"
        ofd.FilterIndex = 2

        If ofd.ShowDialog = DialogResult.Cancel Then Return

        txtDATFile.Text = ofd.FileName
        datFile = ofd.FileName
    End Sub

    Public Shared Function prepforbinaryCollation(ByVal input As String) As String
        Try
            Dim output As String = ""
            Dim conserve As Boolean = False

            For Each s As String In input
                'check to see if string conservation should be turned on or off

                If s = "'" And conserve = False Then
                    conserve = True
                ElseIf s = "'" And conserve = True Then
                    conserve = False
                End If

                If conserve = False Then
                    output &= s.ToLower
                Else
                    output &= s
                End If
            Next

            Return output
        Catch ex As Exception
            Return input
        End Try
    End Function

    Public Function ConvertToFieldType(ByVal SQL As String, ByVal iCon As ADODB.Connection,
   ByVal sColumn As String, ByVal sValue As Object) As Object
        Try
            Dim oField As ADODB.Field
            Dim oType As ADODB.DataTypeEnum
            Dim oRs As ADODB.Recordset
            Dim sFrom() As String

            Dim n As Integer = SQL.ToLower.IndexOf("from")

            SQL = SQL.Substring(n, SQL.Length - n)

            SQL = "SELECT * " & SQL

            oRs = New ADODB.Recordset

            oRs.Open(SQL, iCon, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

            Try
                sColumn = sColumn.Split(".")(1)
            Catch : End Try

            oField = oRs(sColumn)

            oType = oField.Type

            oRs.Close()

            Select Case oType
                Case ADODB.DataTypeEnum.adBigInt, ADODB.DataTypeEnum.adDecimal,
                ADODB.DataTypeEnum.adDouble, ADODB.DataTypeEnum.adInteger, ADODB.DataTypeEnum.adNumeric,
                ADODB.DataTypeEnum.adSingle, ADODB.DataTypeEnum.adSmallInt, ADODB.DataTypeEnum.adTinyInt,
                ADODB.DataTypeEnum.adVarNumeric
                    Return IsNull(sValue, 0)
                Case ADODB.DataTypeEnum.adDate, ADODB.DataTypeEnum.adDBDate, ADODB.DataTypeEnum.adDBTime, ADODB.DataTypeEnum.adDBTimeStamp
                    Return "'" & ConDateTime(IsNull(sValue, Now)) & "'"
                Case Else
                    sValue = SQLPrepare(IsNull(sValue, ""))

                    Return "'" & sValue & "'"
            End Select
        Catch ex As Exception
            Try
                Int32.Parse(sValue)

                Return sValue
            Catch
                sValue = SQLPrepare(sValue)

                Return "'" & sValue & "'"
            End Try
        End Try
    End Function

    Public Function IsNull(ByVal sIn As Object, Optional ByVal sDefault As Object = "") As Object
        Dim Temp As Object

        Try
            Temp = sIn & ""

            If Temp.length > 0 Then
                Return Temp
            Else
                Return sDefault
            End If
        Catch
            Return sIn
        End Try
    End Function

    Public Function ConDateTime(ByVal iDate As Date) As String
        Return ConDate(iDate) & " " & ConTime(iDate)
    End Function

    Public Function ConTime(ByVal iTime As Date) As String
        'ConTime = Microsoft.VisualBasic.Format(iTime, "hh:mm")
        Try
            ConTime = iTime.ToString("HH:mm:ss")
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function
    Public Function ConDate(ByVal iDate As Date) As String
        On Error Resume Next
        Dim nYear1 As Integer
        Dim nYear2 As Integer

        ConDate = iDate.ToString("yyyy-MM-dd")

    End Function

    Public Function SQLPrepare(ByVal sIn As Object) As String
        On Error Resume Next

        If sIn Is Nothing Then
            sIn = String.Empty
        End If

        Return sIn.ToString.Replace("'", "''")
    End Function

    Private Sub ChkUseCustomAuth_Checked(sender As Object, e As RoutedEventArgs) Handles chkUseCustomAuth.Checked, chkUseCustomAuth.Unchecked
        lblUsername.IsEnabled = chkUseCustomAuth.IsChecked
        lblPassword.IsEnabled = chkUseCustomAuth.IsChecked
        txtUsername.IsEnabled = chkUseCustomAuth.IsChecked
        txtPassword.IsEnabled = chkUseCustomAuth.IsChecked
    End Sub

    Private Sub worker_DoWork(sender As Object, e As DoWorkEventArgs) Handles worker.DoWork
        conStringDAT = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & datFile & ";Persist Security Info=False"
        conStringLOCAL = "Provider=SQLNCLI11;Password=cssAdmin12345$;Persist Security Info=True;User ID=sa;Initial Catalog=" & database & ";Data Source=" & server & ";DataTypeCompatibility=80;"

        If CopySystemData(username, password, conStringDAT, conStringLOCAL) Then
            MsgBox("Migration successful", MsgBoxStyle.Information, "Success")
        Else
            MsgBox("Migration failed", MsgBoxStyle.Exclamation, "Failure")
        End If
    End Sub

    Private Sub worker_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles worker.RunWorkerCompleted
        vmDAT2LOCAL.propProgress = 100
        worker.Dispose()
        btnMigrate.IsEnabled = True
    End Sub
End Class
