﻿' This file is used by Code Analysis to maintain SuppressMessage
' attributes that are applied to this project.
' Project-level suppressions either have no target or are given
' a specific target and scoped to a namespace, type, member, etc.

<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>", Scope:="member", Target:="~M:babayaga.PackVersionInfo.clsDataAccess.getRecordsODBC(System.String)~System.Data.DataTable")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>", Scope:="member", Target:="~M:babayaga.PackVersionInfo.clsDataAccess.getRecordsSQL(System.String)~System.Data.DataTable")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>", Scope:="member", Target:="~M:babayaga.PackVersionInfo.clsDataAccess.getRecordsDAT(System.String)~System.Data.DataTable")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>", Scope:="type", Target:="~T:babayaga.PackVersionInfo.clsDataAccess")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>", Scope:="member", Target:="~M:babayaga.ucEncryptDecrypt.btnExit_Click(System.Object,System.Windows.RoutedEventArgs)")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>", Scope:="member", Target:="~P:babayaga.viewModelucEncryptDecrypt.propDecryptInput")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>", Scope:="member", Target:="~P:babayaga.viewModelucExtendo.propServerName")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>", Scope:="member", Target:="~P:babayaga.viewModelucExtendo.propProg")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>", Scope:="member", Target:="~P:babayaga.viewModelucExtendo.propEncodedInfo")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>", Scope:="type", Target:="~T:babayaga.PackVersionInfo.clsTrialDetails")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>", Scope:="type", Target:="~T:babayaga.ucEncryptDecrypt")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>", Scope:="member", Target:="~M:babayaga.ucExtendo.btnConfirm_Click(System.Object,System.Windows.RoutedEventArgs)")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>", Scope:="member", Target:="~M:babayaga.ucExtendo.btnCopyCode_Click(System.Object,System.Windows.RoutedEventArgs)")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>", Scope:="member", Target:="~M:babayaga.ucExtendo.btnExit_Click(System.Object,System.Windows.RoutedEventArgs)")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>", Scope:="type", Target:="~T:babayaga.viewModelucEncryptDecrypt")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>", Scope:="type", Target:="~T:babayaga.viewModelucExtendo")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>", Scope:="type", Target:="~T:babayaga.clsDataAccess")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0067:Dispose objects before losing scope", Justification:="<Pending>", Scope:="member", Target:="~M:babayaga.SimpleEncryption.clsEncryption.EncryptStringAES(System.String)~System.String")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0067:Dispose objects before losing scope", Justification:="<Pending>", Scope:="member", Target:="~M:babayaga.SimpleEncryption.clsEncryption.DecryptStringAES(System.String)~System.String")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>", Scope:="type", Target:="~T:babayaga.clsTrialDetails")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0017:Simplify object initialization", Justification:="<Pending>", Scope:="member", Target:="~M:babayaga.ucExtendo.btnConfirm_Click(System.Object,System.Windows.RoutedEventArgs)")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>", Scope:="type", Target:="~T:babayaga.clsDataMigrator.dbType")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>", Scope:="member", Target:="~P:babayaga.clsDataMigrator.propDatabaseType")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>", Scope:="member", Target:="~P:babayaga.clsDataMigrator.propTablesToInclude")>
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>", Scope:="type", Target:="~T:babayaga.clsDataMigrator")>